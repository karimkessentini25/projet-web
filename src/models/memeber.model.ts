export interface Member {
  id: number,
  cin: string,
  nom: string,
  prenom: string,
  date: any,
  photo: any,
  cv: string,
  email: string,
  password: string,
  type: string,
  publications?: any,
  evenements?: any,
  outils?: any,
  grade?: any,
  etablissement?: any,
  listEtud?: any,
  encadrant?: any,
  role?: string
}

export enum Role {
  ADMIN = 'admin',
  MEMBER = 'member'
}
