import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Member } from 'src/models/memeber.model';
import { AuthentificationService } from 'src/services/authentification.service';
import { MemberService } from 'src/services/member.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public listUsers: Member[] ;
  form: FormGroup = new FormGroup({
    login: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  error: string;

  constructor(
    private authentificationService: AuthentificationService,
    private memberService :MemberService ,
    private route: Router
    ) { }

  ngOnInit(): void {
    this.fetchDataSource();
  }

  private fetchDataSource(): void {
    this.memberService.getAllMembers().then(data => this.listUsers = data);
  }

  submit() {
    if (this.form.valid) {
      this.authentificationService.login(this.form.get('login').value, this.form.get('password').value).then(user => {
        this.route.navigate(['dashboard']);
      }).catch(error => {
        this.error = error;
      });
      
    }
  }

}
