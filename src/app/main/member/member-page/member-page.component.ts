import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Member } from 'src/models/memeber.model';
import { MemberService } from 'src/services/member.service';

@Component({
  selector: 'app-member-page',
  templateUrl: './member-page.component.html',
  styleUrls: ['./member-page.component.scss']
})
export class MemberPageComponent implements OnInit {
  currentItemId: number;
  item: Member;
  displayedColumns: string[] = ['id', 'cin', 'nom', 'prenom','email'];
  dataSource: Member[] ;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private memberService: MemberService,
  ) { }

  ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    if (!!this.currentItemId) {
      this.memberService.getMemberById(this.currentItemId).then(item => {
        this.item = item;
      });
    } else {;
    }
  }

  fetchListEtud() {
    if(!this.isStudent()){
      this.dataSource = this.item.listEtud;
    }else{
      this.dataSource = [];
    }
  }

  isStudent() {
    if(this.item.type == 'student'){
      return true;
    }else{
      return false;
    }
  }

}
