import { Component, OnInit } from '@angular/core';
import { Member } from 'src/models/memeber.model';
import { Tool } from 'src/models/tool.model';
import { Article } from 'src/models/article.model';
import { Event } from 'src/models/event.model';
import { AuthentificationService } from 'src/services/authentification.service';
import {MemberService} from "src/services/member.service";
import { ProfileService } from 'src/services/profile.service';
import { ToolService } from 'src/services/tool.service';

@Component({
  selector: 'app-member-profile',
  templateUrl: './member-profile.component.html',
  styleUrls: ['./member-profile.component.scss']
})
export class MemberProfileComponent implements OnInit {
  public connectedUser: Member ;

  displayedColumns: string[] = ['id', 'cin', 'nom', 'prenom', 'email', 'password', 'Role'];
  displayedArticlesColumns: string[] = ['id', 'titre', 'type', 'dateApparition','lien','sourcePdf', 'actions'];
  displayedToolsColumns: string[] = ['id', 'date', 'source', 'actions'];
  displayedEventsColumns: string[] = ['id', 'title', 'date', 'lieu', 'actions'];

  dataSource: Member[] = [];
  tools: Tool[] = [];
  articles: Article[] ;
  events: Event[] ;

  constructor(
    private profileService :ProfileService,
    private authentificationService: AuthentificationService
    ) { }

  ngOnInit(): void {
    this.checkConnected();
    this.fetchDataSource();
    this.fetchArticles();
    this.fetchTools();
    this.fetchEvents();
    
  }

  private fetchDataSource(): void {
    //this.profileService.getAllTools(this.connectedUser.id).then(data => this.tools = data);
    this.dataSource=[this.connectedUser];
  }

  private fetchArticles(): void {
    this.profileService.getAllArticles(this.connectedUser.id).then(data => this.articles = data);
    
  }

  private fetchTools(): void {
    this.profileService.getAllTools(this.connectedUser.id).then(data => this.tools = data);
    
  }

  private fetchEvents(): void {
    this.profileService.getAllEvents(this.connectedUser.id).then(data => this.events = data);
    
  }

  checkConnected(): void {
    const user: Member = JSON.parse(localStorage.getItem('connectedUser'));
    if (user) {
      this.connectedUser = user;
    } else {
      this.connectedUser = null;
    }
  }
}
