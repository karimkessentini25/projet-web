import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MemberService} from "../../../../services/member.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Member} from "../../../../models/memeber.model";

@Component({
  selector: 'app-member-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.scss']
})
export class MemberFormComponent implements OnInit {
  currentItemId: number;
  item: Member;
  form: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private memberService: MemberService,
  ) {
  }

  ngOnInit(): void {
    this.currentItemId = this.activatedRoute.snapshot.params.id;
    if (!!this.currentItemId) {
      this.memberService.getMemberById(this.currentItemId).then(item => {
        this.item = item;
        this.initForm(item)
      });
    } else {
      this.initForm(null);
    }
  }

  initForm(item: Member) {
    this.form = new FormGroup({
      id: new FormControl(item?.id, [Validators.required]),
      cin: new FormControl(item?.cin, [Validators.required]),
      nom: new FormControl(item?.nom, [Validators.required]),
      prenom: new FormControl(item?.prenom, [Validators.required]),
      date: new FormControl(item?.date, []),
      photo: new FormControl(item?.photo, []),
      cv: new FormControl(item?.cv, []),
      email: new FormControl(item?.email, [Validators.required]),
      password: new FormControl(item?.password, [Validators.required]),
      role: new FormControl(item?.role, [Validators.required]),
      type: new FormControl(item?.type, [Validators.required]),
    });
  }


  isFormInEditMode(): boolean {
    return !!this.currentItemId;
  }

  onSubmit(): void {
    const objectToSubmit: Member = {...this.item, ...this.form.value};
    console.log(objectToSubmit);
    if(this.isFormInEditMode()){
      if(objectToSubmit.type === 'student'){
        this.memberService.saveStudent(objectToSubmit).then(() => this.router.navigate(['./members']));
      }else{
        this.memberService.saveTeacher(objectToSubmit).then(() => this.router.navigate(['./members']));
      }
    }else{
      if(objectToSubmit.type === 'student'){
        this.memberService.saveStudent(objectToSubmit).then(() => this.router.navigate(['./members']));
      }else{
        this.memberService.saveTeacher(objectToSubmit).then(() => this.router.navigate(['./members']));
      }
    }
    
    

  }

}
