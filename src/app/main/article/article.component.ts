import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/@root/components/confirm-dialog/confirm-dialog.component';
import { Article } from 'src/models/article.model';
import { Member } from 'src/models/memeber.model';
import { ArticleService } from 'src/services/article.service';
import { AuthentificationService } from 'src/services/authentification.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, OnDestroy {
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  public connectedUser: Member = null;
  public ideut: number ;
  displayedColumns: string[] = ['id', 'titre', 'type', 'dateApparition', 'lien', 'sourcePdf','actions'];
  dataSource: Article[];

  constructor(private articleService: ArticleService,
    private dialog: MatDialog,
    public authentificationService: AuthentificationService
    ) { }

  ngOnInit(): void {
    this.checkConnected();
    this.fetchDataSource();
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private fetchDataSource(): void {
    this.articleService.getAllArticles().then(data => this.dataSource = data);
  }

  onAddArticle(idpub: any): void {
    this.articleService.addArticleToMember(this.ideut.toString(), idpub);
  }

  onRemoveAccount(id: any): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });

    dialogRef.componentInstance.confirmButtonColor = 'warn';

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.articleService.removeArticleById(id).then(() => this.fetchDataSource());
      }
    });
  }

  checkConnected(): void {
    const user: Member = JSON.parse(localStorage.getItem('connectedUser'));
    if (user) {
      this.connectedUser = user;
      this.ideut = this.connectedUser.id;
    } else {
      this.connectedUser = null;
    }
  }

}
