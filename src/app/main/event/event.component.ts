import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/@root/components/confirm-dialog/confirm-dialog.component';
import { Event } from 'src/models/event.model';
import { EventService } from 'src/services/event.service';
import { AuthentificationService } from 'src/services/authentification.service';
import { Member } from 'src/models/memeber.model';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit, OnDestroy {
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  public connectedUser: Member = null;
  public ideut: number ;
  
  displayedColumns: string[] = ['id', 'title', 'date', 'lieu','actions'];
  dataSource: Event[];

  constructor(private eventService: EventService,
    private dialog: MatDialog,
    public authentificationService: AuthentificationService
    ) { }

  ngOnInit(): void {
    this.checkConnected();
    this.fetchDataSource();
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private fetchDataSource(): void {
    this.eventService.getAllEvents().then(data => this.dataSource = data);
  }

  onAddEvent(idpub: any): void {
   
    this.eventService.addEventToMember(this.ideut, idpub);
  }

  onRemoveAccount(id: any): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: false,
    });

    dialogRef.componentInstance.confirmButtonColor = 'warn';

    dialogRef.afterClosed().pipe(takeUntil(this._onDestroy)).subscribe(isDeleteConfirmed => {
      console.log('removing: ', isDeleteConfirmed);
      if (isDeleteConfirmed) {
        this.eventService.removeEventById(id).then(() => this.fetchDataSource());
      }
    });
  }

  checkConnected(): void {
    const user: Member = JSON.parse(localStorage.getItem('connectedUser'));
    if (user) {
      this.connectedUser = user;
      this.ideut = this.connectedUser.id;
    } else {
      this.connectedUser = null;
    }
  }

}
