import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {GLOBAL} from "../app/app-config";
import {Utils} from "../utils/utils";
import {Tool} from "../models/tool.model";
import { Article } from 'src/models/article.model';
import { Member } from 'src/models/memeber.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public placeholderArticles: Article[];

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllArticles(): Promise<Article[]> {
    return this.httpClient.get<Article[]>('http://192.168.1.12:9999/publicationservice/publications').toPromise();
    //return new Promise(resolve => resolve(this.placeholderArticles));
  }

  getArticleById(id: number): Promise<Article> {
    return this.httpClient.get<Article>('http://192.168.1.12:9999/publicationservice/publications/'+id).toPromise();

  }


  saveArticle(article: any): Promise<Article> {
    return this.httpClient.put<Article>('http://192.168.1.12:9999/publicationservice/publications/publication/'+article.id, article).toPromise();

  }

  removeArticleById(id: number): Promise<void> {
    return this.httpClient.delete<void>('http://192.168.1.12:9999/publicationservice/publications/'+id).toPromise();

  }
  
  addArticleToMember(idpub: string, ideut: string): Promise<any> {
    //return this.httpClient.post<void>('http://192.168.1.12:9999/memberservice/members/auteur?ideut='+ideut+"&idpub="+idpub, {}).toPromise();
    const params1 = new HttpParams()
      .set('ideut', ideut);
    const params2 = new HttpParams()
      .set('idpub', idpub);
    return this.httpClient.put<Member>('http://localhost:9999/memberservice/members/auteur', {}, { params: {ideut, idpub }}).toPromise();
}

}
