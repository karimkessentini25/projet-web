import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GLOBAL} from "../app/app-config";
import {Utils} from "../utils/utils";
import {Tool} from "../models/tool.model";

@Injectable({
  providedIn: 'root'
})
export class ToolService {
  public placeholderTools: Tool[] ;

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllTools(): Promise<Tool[]> {
    return this.httpClient.get<Tool[]>('http://192.168.1.12:9999/outilservice/outils').toPromise();
  
  }

  getToolById(id: number): Promise<Tool> {
    return this.httpClient.get<Tool>('http://192.168.1.12:9999/outilservice/outil/'+id).toPromise();

  }

  /**
   * create a new tool or update an old tool.
   * a new tool doesn't have an id
   */
  createTool(tool: any): Promise<Tool> {
    return this.httpClient.post<Tool>('http://192.168.1.12:9999/outilservice/outils/'+tool.id, tool).toPromise();

  }


  saveTool(tool: any): Promise<Tool> {
    return this.httpClient.put<Tool>('http://192.168.1.12:9999/outilservice/outils/'+tool.id, tool).toPromise();
   
  }

  removeToolById(id: number): Promise<void> {
    return this.httpClient.delete<void>('http://192.168.1.12:9999/outilservice/outils/'+id).toPromise();
  
  }

  addToolToMember(idpub: number, ideut: number): Promise<void> {
    return this.httpClient.post<void>('http://192.168.1.12:9999/memberservice/members/auteur?ideut='+ideut+"&idpub="+idpub, {}).toPromise();
} //ToDo

}
