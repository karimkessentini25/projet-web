import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { GLOBAL } from "../app/app-config";
import { Utils } from "../utils/utils";
import { Member } from "../models/memeber.model";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  public placeholderMembers: Member[];

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllMembers(): Promise<Member[]> {
    return this.httpClient.get<Member[]>(environment.apiURL + 'memberservice/members').toPromise();
    //return new Promise(resolve => resolve(this.placeholderMembers));
  }

  getMemberById(id: number): Promise<Member> {
    return this.httpClient.get<Member>(environment.apiURL+'memberservice/members/'+id).toPromise();
    //return new Promise(resolve => resolve(
    //this.placeholderMembers.filter(item => item.id === id)[0] ?? null
    //));
  }

  /**
   * create a new member or update an old member.
   * a new member doesn't have an id
   */
  saveTeacher(member: any): Promise<Member> {
    return this.httpClient.put<Member>(environment.apiURL+'memberservice/members/enseignant/'+member.id, member).toPromise();
    
  }

  saveStudent(member: any): Promise<Member> {
    return this.httpClient.put<Member>(environment.apiURL+'memberservice/members/etudiant/'+member.id, member).toPromise();
    
  }

  createTeacher(member: any): Promise<Member> {
    return this.httpClient.post<Member>(environment.apiURL+'memberservice/members/enseignant/'+member.id, member).toPromise();
    
  }

  createStudent(member: any): Promise<Member> {
    return this.httpClient.post<Member>(environment.apiURL+'memberservice/members/etudiant/'+member.id, member).toPromise();
    
  }

  removeMemberById(id: number): Promise<void> {
    return this.httpClient.delete<void>(environment.apiURL+'memberservice/members/'+id).toPromise();
  }

}
