import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GLOBAL} from "../app/app-config";
import {Utils} from "../utils/utils";
import {Tool} from "../models/tool.model";
import { Article } from 'src/models/article.model';
import { Event } from 'src/models/event.model';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  public placeholderEvents: Event[];

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllEvents(): Promise<Event[]> {
    return this.httpClient.get<Event[]>('http://192.168.1.12:9999/evenementservice/evenements').toPromise();
  }

  getEventById(id: number): Promise<Event> {
    return this.httpClient.get<Event>('http://192.168.1.12:9999/evenementservice/evenements/'+id).toPromise();

  }


  saveEvent(event: any): Promise<Event> {
    return this.httpClient.put<Event>('http://192.168.1.12:9999/evenementservice/evenements/'+event.id, event).toPromise();

  }

  removeEventById(id: number): Promise<void> {
    return this.httpClient.delete<void>('http://192.168.1.12:9999/evenementservice/evenements/'+id).toPromise();
   
  }

  addEventToMember(idpub: number, ideut: number): Promise<void> {
    return this.httpClient.post<void>('http://192.168.1.12:9999/memberservice/members/auteur?ideut='+ideut+"&idpub="+idpub, {}).toPromise();
} //ToDo

}
