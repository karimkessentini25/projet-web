import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GLOBAL} from "../app/app-config";
import {Utils} from "../utils/utils";
import {Tool} from "../models/tool.model";
import { Article } from 'src/models/article.model';
import { Event } from 'src/models/event.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  public placeholderTools: Tool[] ;

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getAllTools(id: number): Promise<Tool[]> {
    return this.httpClient.get<Tool[]>('http://192.168.1.12:9999/memberservice/outils/developpeur/'+id).toPromise();
    //return new Promise(resolve => resolve(this.placeholderTools));
  }

  getAllArticles(id: number): Promise<Article[]> {
    return this.httpClient.get<Article[]>('http://192.168.1.12:9999/memberservice/publications/auteur/'+id).toPromise();
    //return new Promise(resolve => resolve(this.placeholderTools));
  }

  getAllEvents(id: number): Promise<Event[]> {
    return this.httpClient.get<Event[]>('url'+id).toPromise();  //toDO
    
  }

  //tools

  getToolById(id: number): Promise<Tool> {
    return this.httpClient.get<Tool>('http://192.168.1.12:8089/outils/'+id).toPromise();

  }


  createTool(tool: any): Promise<Tool> {
    return this.httpClient.post<Tool>('http://192.168.1.12:8089/outil/'+tool.id, tool).toPromise();

  }


  saveTool(tool: any): Promise<Tool> {
    return this.httpClient.put<Tool>('http://192.168.1.12:8089/outil/'+tool.id, tool).toPromise();

  }

  removeToolById(id: number): Promise<void> {
    return this.httpClient.delete<void>('http://192.168.1.12:8089/outils/'+id).toPromise();
    
  }

  //articles

  getArticleById(id: number): Promise<Article> {
    return this.httpClient.get<Article>('http://192.168.1.12:9999/publicationservice/publications/'+id).toPromise();

  }


  saveArticle(article: any): Promise<Article> {
    return this.httpClient.put<Article>('http://192.168.1.12:9999/publicationservice/publications/publication/'+article.id, article).toPromise();

  }

  removeArticleById(id: number): Promise<void> {
    return this.httpClient.delete<void>('http://192.168.1.12:9999/publicationservice/publications/'+id).toPromise();

  }

  //events
  
  getEventById(id: number): Promise<Event> {
    return this.httpClient.get<Event>('http://192.168.1.12:8088/evenements/'+id).toPromise();

  }


  saveEvent(event: any): Promise<Event> {
    return this.httpClient.put<Event>('http://192.168.1.12:8088/evenements/'+event.id, event).toPromise();

  }

  removeEventById(id: number): Promise<void> {
    return this.httpClient.delete<void>('http://192.168.1.12:8088/evenements/'+id).toPromise();
   
  }

}
